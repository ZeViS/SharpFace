﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClickHouse.Ado;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace SharpFace.Tests.Windows
{
    class Program
    {
        static void Main(string[] args)
        {

            OpenCvSharp.Windows.NativeBindings.Init();
            SharpFace.Windows.Native.Init();
            //foreach (var file in File.ReadAllLines(@"C:\Users\vszenin.MAINDOMAIN\Downloads\VK-Scraper-master\faces.txt")
            //)
            //{
            //    HashSet.Add(file);
            //}

            var factory = new ConnectionFactory()
                {Port = 5672, HostName = "185.224.248.237", UserName = "hui", Password = "znaet123"};
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "vk_obj",
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    channel.BasicQos(prefetchSize: 0, prefetchCount: 5, global: false);


                    //for (int i = 0; i < 1000000; i++)
                    //    channel.BasicPublish(exchange: "",
                    //        routingKey: "vk_img",
                    //        basicProperties: null,
                    //        body: Encoding.UTF8.GetBytes(i.ToString()));

                    ConcurrentQueue<IEnumerable> points = new ConcurrentQueue<IEnumerable>();
                    var writer = new Thread(() =>
                    {
                        var list = new List<IEnumerable>();
                        var connection2 = new ClickHouseConnection(
                            "Compress=True;CheckCompressedHash=False;Compressor=lz4;Host=185.224.248.237;Port=9000;Database=default;User=default");
                        connection2.Open();
                        while (true)
                        {
                            Thread.Sleep(2000);
                            while (points.TryDequeue(out var result))
                            {
                                list.Add(result);
                                if (list.Count < 100)
                                    continue;


                                var command = connection2.CreateCommand();
                                command.CommandText =
                                    "INSERT INTO default.face_criterion (event_date, id_account, criterion_1, criterion_2, criterion_3, criterion_4, criterion_5, criterion_6, criterion_7, criterion_8, criterion_9) VALUES @bulk";

                                command.Parameters.Add(new ClickHouseParameter {ParameterName = "bulk", Value = list});

                                command.ExecuteNonQuery();

                                list = new List<IEnumerable>();
                                break;
                            }
                        }
                    }) {IsBackground = true};
                    writer.Start();

                    Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
                    LandmarkWrapperTest t =
                        //new LandmarkTestVid();
                        new LandmarkWrapperTest(points, (path) =>
                            {
                                System.IO.Directory.CreateDirectory(Path.GetDirectoryName(path));
                            }, (str) => { HashSet.Add(str); }, () => { Thread.Sleep(100); },
                            File.Delete, Console.WriteLine, (p, c) =>
                            {
                                Directory.CreateDirectory(Path.GetDirectoryName(p));
                                File.WriteAllLines(p, new[] {c});
                            },
                            (a) =>
                            {
                                (new Thread(() => { a(); })
                                        {IsBackground = true, Priority = ThreadPriority.AboveNormal})
                                    .Start();
                            });


                    t.Run();
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        t.Enqueue(JsonConvert.DeserializeObject<MediaInfo>(Encoding.UTF8.GetString(ea.Body)));
                        Console.WriteLine(" [x] Received {0}", Encoding.UTF8.GetString(ea.Body));
                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: true);
                    };
                    channel.BasicConsume(queue: "vk_obj",
                        autoAck: false,
                        consumer: consumer);


                    Console.Write($"======== Test Finished =======");
                    while (Console.ReadLine() == null)
                    {
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        public static HashSet<string> HashSet = new HashSet<string>();

        public static IEnumerable<string> GetFiles2()
        {
            foreach (string file in Directory.EnumerateFiles(
                @"C:\Users\vszenin.MAINDOMAIN\Downloads\VK-Scraper-master\res", "*_*.jpg", SearchOption.AllDirectories))
            {
                if (File.Exists(file.Replace(@"VK-Scraper-master\res", @"VK-Scraper-master\res_txt").Replace(".jpg", ".txt")))
                //{
                //    HashSet.Add(file.Replace(".jpg", ".txt"));
                    continue;
                //}

                int res;
                if (int.TryParse(file.Split('_').Last().Replace(".jpg", ""), out res))
                    yield return file;
            }
        }

        public static IEnumerable<string> GetFiles()
        {
            foreach (string file in Directory.EnumerateFiles(
                @"C:\Users\vszenin.MAINDOMAIN\Downloads\VK-Scraper-master\res", "*.*", SearchOption.AllDirectories))
            {
                if (HashSet.Contains(file))
                    continue;
                yield return file;
            }
        }
    }
}
