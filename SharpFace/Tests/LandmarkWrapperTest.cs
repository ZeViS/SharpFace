﻿using OpenCvSharp;
using OpenCvSharp.Native;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SharpFace.Tests
{
    public class MediaInfo
    {
        public string fotoUrl { get; set; }

        public string accountId { get; set; }


    }

    public class LandmarkWrapperTest : TestBase
    {
        private readonly ConcurrentQueue<IEnumerable> _points;
        private readonly Action<string> _createDirectory;
        private readonly Action<string> _addToHash;
        private readonly Action _sleep;
        private readonly Action<string> _delete;
        private readonly Action<string> writecons;
        private readonly Action<string, string> _writeLandmarks;
        private readonly string _modelPath;
        [ThreadStatic]
        public static LandmarkDetectorWrap wrap;
        [ThreadStatic]
        static CascadeClassifier cascade;

        public LandmarkWrapperTest(ConcurrentQueue<IEnumerable> points, Action<string> create_directory, Action<string> add_to_hash, Action sleep, Action<string> delete, Action<string> writecons, Action<string, string> write_landmarks, Action<Action> thrd,  int index = 0,  string modelPath = "./")
        {
            _points = points;
            _createDirectory = create_directory;
            _addToHash = add_to_hash;
            _sleep = sleep;
            _delete = delete;
            this.writecons = writecons;
            _writeLandmarks = write_landmarks;
            _modelPath = modelPath;
            
            thrd(ImProc);
            thrd(write);
            thrd(ImProc);
            thrd(ImProc);
            //thrd(NewMethod);
            //thrd(NewMethod);
        }

        public void Enqueue(MediaInfo file)
        {
            while (_mats.Count > 100)
            {
                _sleep();
            }

            //var frame = new Mat(file);
            try
            {
                HttpClient client = new HttpClient();
                using (Stream inputStream = client.GetStreamAsync(file.fotoUrl).Result)
                using (Stream outputStream = new MemoryStream())
                {
                    byte[] buffer = new byte[262144];
                    int bytesRead;
                    do
                    {
                        bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                        outputStream.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                    var frame = Mat.FromStream(outputStream, ImreadModes.Unchanged);
                    _mats.Enqueue(new Tuple<Mat, MediaInfo>(frame, file));
                }
            }
            catch
            {

            }
        }

        private void ImProc()
        {
            wrap = new LandmarkDetectorWrap(_modelPath);
            cascade = new CascadeClassifier();

            cascade.Load(wrap.Parameters.face_detector_location);

            wrap.Load();
            wrap.InVideo = false;
            while (true)
            {
                Tuple<Mat, MediaInfo> frame;
                if (_mats.TryDequeue(out frame))
                {
                    Proc(frame.Item1, new FrameArgs(frame.Item1), frame.Item2);
                    frame.Item1.Dispose();
                    GC.SuppressFinalize(frame.Item1);
                    //_delete(frame.Item2);
                }
                else
                {
                    _sleep();
                }
            }
        }

        private void write()
        {
            while (true)
            {
                Tuple<Mat, string> frame;
                if (_writeMats.TryDequeue(out frame))
                {
                    _createDirectory(frame.Item2);
                    frame.Item1.SaveImage(frame.Item2);
                    frame.Item1.Dispose();
                    //writecons(frame.Item2);
                    //_addToHash(frame.Item2);
                }
                else
                {
                    _sleep();
                }
            }
        }

        private ConcurrentQueue<Tuple<Mat, MediaInfo>> _mats = new ConcurrentQueue<Tuple<Mat, MediaInfo>>();
        ConcurrentQueue<Tuple< Mat, string>> _writeMats = new ConcurrentQueue<Tuple<Mat, string>>();

        public override void Dispose()
        {
            if (wrap != null)
            {
                wrap.Dispose();
                wrap = null;
            }
        }

        public override void Start()
        {
        }

        public override void Stop()
        {
        }

        public override void Wait()
        {
        }

        private void DrawInROI(Mat mat, Rect roi, List<Point2d> pt)
        {
            foreach (var p in pt)
            {
                mat.DrawMarker((int)(roi.X + p.X), (int)(roi.Y + p.Y), Scalar.Red, MarkerStyle.CircleAndCross, 10, LineTypes.AntiAlias);
            }
        }

        private void DrawInfo(Mat mat, Point2d pt)
        {
            var rot = wrap.EularRotation;
            var pos = wrap.Transform;
            var fmt = "0.000";
            mat.PutText($"R:{rot[0].ToString(fmt)},{rot[1].ToString(fmt)},{rot[2].ToString(fmt)}", new Point(pt.X, pt.Y), HersheyFonts.HersheyPlain, 1, Scalar.Lime, 1, LineTypes.AntiAlias);
            mat.PutText($"T:{pos[0].ToString(fmt)},{pos[1].ToString(fmt)},{pos[2].ToString(fmt)}", new Point(pt.X, pt.Y+20), HersheyFonts.HersheyPlain, 1, Scalar.Lime, 1, LineTypes.AntiAlias);
        }

        public class Row : IEnumerable
        {
            public DateTime date;
            public string source;
            public double[] criterions = new double[198];

            public IEnumerator GetEnumerator()
            {
                yield return date;
                yield return source;
                foreach (var criterion in criterions)
                    yield return criterion;
            }
        }

        public double Distance3D(double x1, double y1, double z1, double x2, double y2, double z2)
        {
            //     __________________________________
            //d = √ (x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2
            //


            //Our end result
            int result = 0;
            //Take x2-x1, then square it
            double part1 = Math.Pow((x2 - x1), 2);
            //Take y2-y1, then sqaure it
            double part2 = Math.Pow((y2 - y1), 2);
            //Take z2-z1, then square it
            double part3 = Math.Pow((z2 - z1), 2);
            //Add both of the parts together
            double underRadical = part1 + part2 + part3;
            //Get the square root of the parts
            //Return our result
            return Math.Sqrt(underRadical);
        }

        long realLastMs = 0;
        long lastMs = 0;
        long time = 0;
        bool onFace = true;
        private void Proc(Mat read, FrameArgs args, MediaInfo file_info)
        {
            if (!read.Empty())
            {
                //Cv2.Resize(read, read, new Size(320, 240));
                try
                {
                    var file_name = file_info.fotoUrl.Split('/').Last();
                    wrap.CheckFocus(read);
                    var faces = cascade.DetectMultiScale(read);
                    for (var index = 0; index < faces.Length; index++)
                    {
                        var face = faces[index];
                        int w = (int) (face.Width * 0.3);
                        int h = (int) (face.Height * 0.4);
                        int sx = Math.Max(face.X - (int) (w * 0.9), 0);
                        int sy = Math.Max(face.Y - (int) (h * 1.3), 0);
                        int wh = face.Width + 2 * w + sx > read.Width ? read.Width - sx : face.Width + 2 * w;
                        int hh = face.Height + 2 * h + sy > read.Height ? read.Height - sy : face.Height + 2 * h;
                        var face2 = new Rect(sx, sy, wh, hh);
                        
                        try
                        {
                            var read2 = new Mat(read, face2);
                            if (_writeMats.Count > 1000)
                                _sleep();

                            wrap.DetectImage(read2);

                            //var scale = 1;
                            //Cv2.Resize(read, read, new Size(read.Width / scale, read.Height / scale));
                            //wrap.Draw(read);
                            //var box = wrap.BoundaryBox;
                            var res = new PDM(wrap.Model.pdm.Pointer, true);
                            var values = new MatOfDouble();
                            res.CalcShape3D(values.ToSwig(), wrap.Model.params_local);
                            values = values.Reshape(1, 3).T();
                            var v = values.ToRectangularArray();
                            var lists = new List<string>();
                            for (int i = 0; i < 68; i++)
                            {
                                lists.Add(
                                    $"[{v[i, 0].ToString().Replace(',', '.')},{v[i + 68, 0].ToString().Replace(',', '.')},{v[i + 68 + 68, 0].ToString().Replace(',', '.')}]");
                                //lists.Add($"{v[i, 0].ToString().Replace(',', '.')};");
                            }

                            values.Dispose();
                            res.Dispose();
                            read2.Dispose();
                            GC.SuppressFinalize(values);
                            GC.SuppressFinalize(res);
                            GC.SuppressFinalize(read2);

                            //_writeMats.Enqueue(new Tuple<Mat, string>(read2, "C:\\Data\\vk\\" + file_info.accountId + "\\" +
                            //                                                 file_name.Replace(".jpg", "_" + index + ".jpg")
                            //                                                     .Replace(".JPG", "_" + index + ".JPG")));

                            var r = new Random();
                            var enumerable = new Row
                            {
                                date = DateTime.Now,
                                source = file_info.accountId,   
                                //c1 = Distance3D(v[1, 0], v[1 + 68, 0], v[1 + 68 + 68, 0], v[2, 0], v[2 + 68, 0], v[2 + 68 + 68, 0]),
                                //c2 = Distance3D(v[2, 0], v[2 + 68, 0], v[2 + 68 + 68, 0], v[3, 0], v[3 + 68, 0], v[3 + 68 + 68, 0]),
                                //c3 = Distance3D(v[3, 0], v[3 + 68, 0], v[3 + 68 + 68, 0], v[4, 0], v[4 + 68, 0], v[4 + 68 + 68, 0]),
                                //c4 = Distance3D(v[4, 0], v[4 + 68, 0], v[4 + 68 + 68, 0], v[5, 0], v[5 + 68, 0], v[5 + 68 + 68, 0]),
                                //c5 = Distance3D(v[5, 0], v[5 + 68, 0], v[5 + 68 + 68, 0], v[6, 0], v[6 + 68, 0], v[6 + 68 + 68, 0]),
                                //c6 = Distance3D(v[6, 0], v[6 + 68, 0], v[6 + 68 + 68, 0], v[7, 0], v[7 + 68, 0], v[7 + 68 + 68, 0]),
                                //c7 = Distance3D(v[7, 0], v[7 + 68, 0], v[7 + 68 + 68, 0], v[8, 0], v[8 + 68, 0], v[8 + 68 + 68, 0]),
                                //c8 = Distance3D(v[8, 0], v[8 + 68, 0], v[8 + 68 + 68, 0], v[9, 0], v[9 + 68, 0], v[9 + 68 + 68, 0]),
                                //c9 = Distance3D(v[9, 0], v[9 + 68, 0], v[9 + 68 + 68, 0], v[10, 0],v[10 + 68, 0], v[10 + 68 + 68, 0])
                            };
                            _points.Enqueue(enumerable);
                            
                            //Task.Factory.StartNew(() =>
                            //{
                            //    //_writeLandmarks(
                            //    //    file_name.Replace(@"VK-Scraper-master\res", @"VK-Scraper-master\res_txt")
                            //    //        .Replace(".jpg", ".txt"), string.Join("", lists));
                            //});

                            //Cv2.Rectangle(read, new Rect((int)box.X, (int)box.Y, (int)box.Width, (int)box.Height), Scalar.Aqua, 2);
                            //DrawInfo(read, new Point2d(10, 20));
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
                catch (Exception e)
                {
                }


                //time = sw.ElapsedMilliseconds - realLastMs;
                //read.PutText($"realfps:{1000/time}", new Point(10, 100), HersheyFonts.HersheyPlain, 1, Scalar.Lime, 1, LineTypes.AntiAlias);
                //read.PutText($"fps:{1000 / (sw.ElapsedMilliseconds - lastMs)}", new Point(10, 120), HersheyFonts.HersheyPlain, 1, Scalar.Lime, 1, LineTypes.AntiAlias);
                //lastMs = sw.ElapsedMilliseconds;
                //Cv2.ImShow("test", read);
            }


            switch (args.LastKey)
            {
                case 'r':
                    wrap.Reset();
                    break;
                case 'i':
                    wrap.InVideo = !wrap.InVideo;
                    break;
                case 'f':
                    onFace = !onFace;
                    break;
                case 'q':
                    args.Break = true;
                    break;
            }
        }
    }
}
